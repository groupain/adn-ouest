# ADN Ouest

**Projet :** 
Développer un outil attractif et participatif afin de montrer la diversité des
métiers du numérique aux cibles I Like IT (citées ci-dessus)

**L’outil devra être :**
- intuitif pour faciliter la navigation du jeune utilisateur (12 ans)
- numérique pour permettre l’utilisation dans toute la région
- facile d’accès pour nos adhérents bénévoles
- exploitable par des intervenant.e.s lors des actions I Like IT (citées ci-dessus) pour faire naitre
    des interactions (discussions, débats)
- opérationnel en classe pour minimum 50 connexions en simultanées
- sans récupération des données personnelles


**Objectifs :**
- Sensibiliser et donner plus de visibilité aux métiers du numérique, informer
  des enjeux et opportunités afin de mettre en avant l’attractivité de notre
  filière qui peine aujourd’hui à recruter autant de talents que nécessaire.
- Aider au rapprochement entre l’écosystème numérique (professionnels)
  et le monde de l’éduction nationale


**Cibles :**
- Collégien.e.s, lycéen.e.s,
- étudiant.e.s (changement de filière),
- décrocheur.se.s scolaire